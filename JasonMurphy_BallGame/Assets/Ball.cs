﻿using UnityEngine;
using System.Collections;
using System;



public class Ball : MonoBehaviour
{

    public float Mass, Speed, Radius;
    private float coeficientOfRestitution = 0.5f;
    public Vector3 velocity, impact, normalOfCol;

    // an array of surface objects to be used to detect collisions
    private SurfaceObject[] surfaces;
    // an array of ball objects to be used to detect collisions
    private Ball[] BallList;
    // used to determine which ball gets which outcome after a collision has happened
    public bool isMain;

    public Vector3 Up { get; set; }

    // Use this for initialization
    void Start()
    {
        // setting the array of balls to every ball object in the game scene
        BallList = GameObject.FindObjectsOfType<Ball>();

        // setting the array surfaces to every surface object in the game scene
        surfaces = GameObject.FindObjectsOfType<SurfaceObject>();

        // the initial velocity of the ball is 0,0,0
        velocity = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        // apply gravity each frame
        velocity += Physics.gravity * Time.deltaTime;

        // for each surface in the scene 
        foreach (SurfaceObject surface in surfaces)
        {
            // check if the ball's radius is less that the distance to the surface
            if (surface.distanceToFloor(transform.position) < this.Radius)
            {
                // minus the velocity of the ball - collision has occured
                transform.position -= velocity * Time.deltaTime;

                // find the new outcome velocity after the collision with the surface
                velocity = (-findParrellel(velocity, surface.Up) * coeficientOfRestitution) + findPerpendicular(velocity, surface.Up);
            }
        }

        // for each ball in the scene
        foreach (Ball b in BallList)
        {
            // check if a collision has occured and that the ball being examined isn't itself
            if (Vector3.Distance(b.transform.position, this.transform.position) < GetRadi(b, this) && this != b)
            {
                // setting two new ball objects for reference
                Ball b1 = new Ball(); 
                Ball b2 = new Ball();
                
                // assigning reference ball objects to the balls that collided
                b1 = this;
                b2 = b;

                CollisionOutcome(ref b1, ref b2);
            }
        }

        // tranform the position of the ball by any velocity changes or just gravity if nothing new has happened.
        transform.position += velocity * Time.deltaTime;
    }

    /// <summary>
    /// Used in collision outcome
    /// </summary>
    /// <param name="b2"></param>
    /// <returns></returns>
    public Vector3 getPointOfImapact(Ball b2)
    {
        // the normalized direction from the opposite ball to this ball
        Vector3 dir = Vector3.Normalize(b2.transform.position - transform.position);
        // returning the impact location vector
        return transform.position + Radius * dir;
    }

    /// <summary>
    /// Used in determining if a colision has happened
    /// </summary>
    /// <param name="b1"></param>
    /// <param name="b2"></param>
    /// <returns></returns>
    public float GetRadi(Ball b1, Ball b2)
    {
        // first ball's radius
        var radius1 = b1.Radius;

        // second ball's radus
        var radius2 = b2.Radius;

        // total of the radius combined
        return radius1 + radius2;

    }

    public static void CollisionOutcome(ref Ball b1, ref Ball b2)
    {
        // normals of collision to point on ball.
        Vector3 normal1 = Vector3.Normalize(b1.transform.position - b2.getPointOfImapact(b1));
        Vector3 normal2 = Vector3.Normalize(b2.transform.position - b1.getPointOfImapact(b2));

        // finding the parrellel using ball 1's velocity and the normalized vector from ball 1 to where the impact occured
        Vector3 Ball1_Parrellel = b1.findParrellel(b1.velocity, normal1);
     
        // finding the parrellel using ball 2's velocity and the normalized vector from ball 2 to where the impact occured
        Vector3 Ball2_Parrellel = b2.findParrellel(b2.velocity, normal2);
        
        // finding the perpendicular using the velocity and from where ball 1 is to where the impact occured 
        Vector3 Ball1_Perpendicular = b1.findPerpendicular(b1.velocity, normal1);

        // find the perpendicular using the velocity and the normalized vector from ball 2 to where the imapact occured 
        Vector3 Ball2_Perpendicular = b2.findPerpendicular(b2.velocity, normal2);

        // push ball back after collision
        b1.transform.position -= b1.velocity * Time.deltaTime;
        b2.transform.position -= b2.velocity * Time.deltaTime;

        // adjust new velocity
        // true to use part one of the formula, false to use part two of the formula
        b1.velocity = b1.ResolveCollision(b1, b2, true, Ball1_Parrellel, Ball2_Parrellel) + Ball1_Perpendicular;
        b2.velocity = b2.ResolveCollision(b1, b2, false, Ball1_Parrellel, Ball2_Parrellel) + Ball2_Perpendicular;

        // tranform the position of ball 1 & 2 using the new velocities.
        b1.transform.position += b1.velocity * Time.deltaTime;
        b2.transform.position += b2.velocity * Time.deltaTime;
    }

    // works
    public Vector3 findParrellel(Vector3 v, Vector3 contact)
    {
        // return the dot product of the velocity and the vector it is contacting with
        return Vector3.Dot(v, contact) * contact;

    }

    /// <summary>
    ///  Finds the perpendicular to non-movable object. Used in wall/floor/roof collosion
    /// </summary>
    /// <param name="v"></param>
    /// <param name="contact"></param>
    /// <returns></returns>
    public Vector3 findPerpendicular(Vector3 v, Vector3 contact)
    {
        // velocity - parrellel
        return v - findParrellel(v, contact);
    }

    /// <summary>
    /// Determines the new velocity after collision. Uses mass and old velocities.
    /// </summary>
    /// <param name="b1"></param>
    /// <param name="b2"></param>
    /// <param name="main"></param>
    /// <param name="u1"></param>
    /// <param name="u2"></param>
    /// <returns></returns>
    public Vector3 ResolveCollision(Ball b1, Ball b2, bool main, Vector3 u1, Vector3 u2)
    {
        if (main) // part 1 of formula
        {
            // newVelocity1 = m1-m2 m1+m2 * oldVelocity1 + 2m2 / m1+m2 * oldVelocity2
            Vector3 outcome = (((b1.Mass - b2.Mass) / (b1.Mass + b2.Mass)* u1) +  (((b2.Mass * 2) / (b1.Mass + b2.Mass)) * u2));
            return outcome * coeficientOfRestitution;
        }
        else // part 2 of formula
        {
            // newVelocity2 =  m2-m1 / m1+m2 * oldVelocity2 + 2m2 / m1+m2 * oldvelocity2 
            Vector3 outcome = (((b2.Mass - b1.Mass) / (b1.Mass + b2.Mass))* u2) +  (((b1.Mass * 2) / (b1.Mass + b2.Mass)) * u1);

            return outcome * coeficientOfRestitution;

        }
    }


}

﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{


    private Vector3 offset, screenPoint;
    GameObject g;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
      
       
    }

    /// <summary>
    /// Movement needed to drag a game object using the mouse
    /// </summary>
    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);

        // sets the postion vector the current location of the mouse
        transform.position = curPosition;

        // while being draged around the screen the ball's velocity remains at 0,0,0
        gameObject.GetComponent<Ball>().velocity = Vector3.zero;
    }

    void OnMouseDown()
    {
        // vector of where the camera is in relation to the pointer - used to transform the game objects position
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        // once picked up the velocity of the ball is reset to 0,0,0
        gameObject.GetComponent<Ball>().velocity = Vector3.zero;

        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

}

﻿using UnityEngine;
using System.Collections;

public class SurfaceObject : MonoBehaviour {

    public Vector3 Up { get; set; }
    // Use this for initialization
    void Start () {
	    Up = transform.up;
        
    }
	
	// Update is called once per frame
	void Update () {
	}

    /// <summary>
    /// Returns the distance from the radius of the sphere to the floor
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public float distanceToFloor(Vector3 point)
    {
        float d = -Vector3.Dot(
                    transform.position, // point on plane
                    transform.up);      // Normal to the plane

        return (Vector3.Dot(transform.up, point) + d);


    }
}

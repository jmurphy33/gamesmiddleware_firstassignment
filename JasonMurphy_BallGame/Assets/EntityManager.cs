﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EntityManager : MonoBehaviour
{
    public GameObject sphere; 

    // to keep track of both spheres in the game
    private GameObject sphereA, sphereB, sphereC;

    // r1 = size1 r2 = size2 m1 = mass1 and m2 = mass2
    private string r1 = "", r2 = "", r3="",  m1 = "", m2 = "", m3 = "", errorMsg = "", z1 = "", z2="",z3 ="";
    

    void OnGUI()
    {
        var style1 = new GUIStyle(); style1.normal.textColor = Color.white;
        var style2 = new GUIStyle(); style2.normal.textColor = Color.blue;
        var style3 = new GUIStyle(); style3.normal.textColor = Color.red;
        // sets the size for sphere A
        //GUI.Box(new Rect(9,10,160, 190), "");
        GUI.Label(new Rect(10, 10, 200, 20), "Size of ball A:",style1);
        r1 = GUI.TextField(new Rect(110, 10, 60, 20), r1, 25);

        // sets the mass for sphere A
        GUI.Label(new Rect(10, 30, 200, 20), "Mass of ball A:",style1);
        m1 = GUI.TextField(new Rect(110, 30, 60, 20), m1, 25);

        // sets the z position for sphere A
        GUI.Label(new Rect(10, 50, 200, 20), "Z Axis of ball A:",style1);
        z1 = GUI.TextField(new Rect(110, 50, 60, 20), z1, 25);

        // sets the size for sphere B
        GUI.Label(new Rect(10, 70, 200, 20), "Size of ball B:", style2);
        r2 = GUI.TextField(new Rect(110, 70, 60, 20), r2, 25);

        // sets the mass for sphere B
        GUI.Label(new Rect(10, 90, 200, 20), "Mass of ball B:", style2);
        m2 = GUI.TextField(new Rect(110, 90, 60, 20), m2, 25);

        GUI.Label(new Rect(10, 110, 200, 20), "Z Axis of ball B:", style2);
        z2 = GUI.TextField(new Rect(110, 110, 60, 20), z2, 25);

        // sets the size for sphere C
        GUI.Label(new Rect(10, 130, 200, 20), "Size of ball C:",style3);
        r3 = GUI.TextField(new Rect(110, 130, 60, 20), r3, 25);

        // sets the mass for sphere C
        GUI.Label(new Rect(10, 150, 200, 20), "Mass of ball C:", style3);
        m3 = GUI.TextField(new Rect(110, 150, 60, 20), m3, 25);

        GUI.Label(new Rect(10, 170, 200, 20), "Z Axis of ball C:", style3);
        z3 = GUI.TextField(new Rect(110, 170, 60, 20), z3, 25);

        // information on sphere A's velocity, mass and radius
        GUI.Label(new Rect(210, 10, 200, 20), "Ball A Velocity: "+ sphereA.GetComponent<Ball>().velocity, style1);
        GUI.Label(new Rect(210, 25, 200, 20), "Ball A Mass: " + sphereA.GetComponent<Ball>().Mass, style1);
        GUI.Label(new Rect(210, 40, 200, 20), "Ball A Radius: " + sphereA.GetComponent<Ball>().Radius, style1);
        GUI.Label(new Rect(210, 60, 200, 20), "Ball A Position: " + sphereA.GetComponent<Ball>().transform.position, style1);

        // information on spere B's velocity, mass and radius
        GUI.Label(new Rect(410, 10, 200, 20), "Ball B Velocity: " + sphereB.GetComponent<Ball>().velocity, style2);
        GUI.Label(new Rect(410, 25, 200, 20), "Ball B Mass: " + sphereB.GetComponent<Ball>().Mass, style2);
        GUI.Label(new Rect(410, 40, 200, 20), "Ball B Radius: " + sphereB.GetComponent<Ball>().Radius, style2);
        GUI.Label(new Rect(410, 60, 200, 20), "Ball A Position: " + sphereB.GetComponent<Ball>().transform.position, style2);

        // information on spere B's velocity, mass and radius
        GUI.Label(new Rect(610, 10, 200, 20), "Ball C Velocity: " + sphereC.GetComponent<Ball>().velocity, style3);
        GUI.Label(new Rect(610, 25, 200, 20), "Ball C Mass: " + sphereC.GetComponent<Ball>().Mass, style3);
        GUI.Label(new Rect(610, 40, 200, 20), "Ball C Radius: " + sphereC.GetComponent<Ball>().Radius, style3);
        GUI.Label(new Rect(610, 60, 200, 20), "Ball A Position: " + sphereC.GetComponent<Ball>().transform.position, style3);

        GUI.Label(new Rect(100, 200, 200, 30), "Move X & Y with Mouse");
        GUI.Label(new Rect(400, 200, 600, 20), "Can only scale from 1 - 10 ");

    }
    // Use this for initialization
    void Start()
    {
        // let sphere A,B and C equal the newly spawned spheres with the given position, color, mass, size 
        sphereA = spawnBall(-50, 15, 0, Color.white, 8, 5);
        sphereB = spawnBall(50, 18, 0, Color.blue, 5, 6);
        sphereC = spawnBall(10, 18, 0, Color.red, 3, 7);

       
    }

    GameObject spawnBall(float x, float y, float z,
        Color color, float mass, float size)
    {
            // spawn a game object of type sphere with the given coordinates and rotation
            var sphere1 = (GameObject)Instantiate(sphere, new Vector3(x, y, z), transform.rotation);

            // alter the size of the sphere by the given size argument
            sphere1.transform.localScale = new Vector3(size, size, size);

           
            // set the radius of the sphere collider to half the size (not needed)
            sphere1.GetComponent<SphereCollider>().radius = 0.5f;

            // set the radius of the ball object to half the size
            sphere1.GetComponent<Ball>().Radius = sphere1.GetComponent<SphereCollider>().radius * sphere1.transform.localScale.x;


            // sets the mass for the ball - used in the outcome of the collision
            sphere1.GetComponent<Ball>().Mass = mass;

            // sets the colour of the ball
            sphere1.GetComponent<Renderer>().material.SetColor("_Color", color);

            // return the ball object to let sphere A or B equal to it...
            return sphere1;
        
       
    }
 
    
    // Update is called once per frame
    void Update()
    {   
        // if there is a change in a GUI element - adjust some aspect of the sphere
        if (GUI.changed)
        {
      
            // if a value has been entered for the size of sphere A alter the object's size
            if (r1 != "" && r1 != null){fixSize(sphereA, r1);}         
            if (r2 != "" && r2 != null) { fixSize(sphereB,r2); }
            if (r3 != "" && r3 != null) {fixSize(sphereC, r3); }

            // if a value has been entered for the z axis to change
            if (z1 != "" && z1 != null) { fixZposition(sphereA, z1); }
            if (z2 != "" && z2!= null) { fixZposition(sphereB, z2); }
            if (z3 != "" && z3 != null) { fixZposition(sphereC, z3); }

            // if a value has been entered for the mass of sphere A alter the object's mass
            if (m1 != "" && m1 != null)  { fixMass(sphereA,m1); }   
            if (m2 != "" && m2 != null){ fixMass(sphereB, m2); }
            if (m3 != "" && m3 != null) {fixMass(sphereC, m3);}
        }
    }

    /// <summary>
    /// Used to alter the Z position of a game object
    /// </summary>
    /// <param name="b1"></param>
    /// <param name="z"></param>
    private void fixZposition(GameObject b1, string z)
    {
        // parsing the string entered into a float variable
        float Zposition = float.Parse(z);
        // setting the velocity to zero to avoid strange movement.
        b1.GetComponent<Ball>().velocity = Vector3.zero;
        // moving the ball to the z coordinate entered.
        b1.transform.position = new Vector3(b1.transform.position.x, b1.transform.position.y, Zposition);
    }

    /// <summary>
    /// Used to alter the mass of the ball
    /// </summary>
    /// <param name="b1"></param>
    /// <param name="mass"></param>
    private void fixMass(GameObject b1, string mass)
    {
        // parsing the mass entered into a float variable
        float m = float.Parse(mass);
        // setting the ball objects mass to the parsed value
        b1.GetComponent<Ball>().Mass = m;
    }

    /// <summary>
    /// Used to alter the size and radius of the sphere.
    /// </summary>
    /// <param name="b1"></param>
    /// <param name="size"></param>
    private void fixSize(GameObject b1, string size)
    {
        try {
            // parsing the scales entered into a float variable
            float scale = float.Parse(size);
            // keeping the scale limited to fit into map correctly
            if (scale < 11 && scale > 0)
            {
                // setting the local scale of the obect to the value entered
                b1.transform.localScale = new Vector3(scale, scale, scale);
                // setting the size of the sphere collider attached to the object to the value entered
                b1.GetComponent<SphereCollider>().transform.localScale = new Vector3(scale, scale, scale);
                // setting the radius of the ball object to that of the sphere collider.
                b1.GetComponent<Ball>().Radius = b1.GetComponent<SphereCollider>().radius * b1.transform.localScale.x; ;
            }
            else
            {
                // if a problem has occured use a standard size of 2
                b1.transform.localScale = new Vector3(2, 2, 2);
                b1.GetComponent<SphereCollider>().transform.localScale = new Vector3(2, 2, 2);
                b1.GetComponent<Ball>().Radius = b1.GetComponent<SphereCollider>().radius * b1.transform.localScale.x;
                errorMsg = "scaled too large...can only scale from 1-10";
            }
        }
        catch (Exception e)
        {
            b1.transform.localScale = new Vector3(2, 2, 2);
            b1.GetComponent<SphereCollider>().transform.localScale = new Vector3(2, 2, 2);
            b1.GetComponent<Ball>().Radius = 2 / 2;
            errorMsg = "invalid...can only scale from 1-10";
        }
    }

   

}
